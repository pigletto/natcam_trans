DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'testdatabase',
    }
}
INSTALLED_APPS = ['natcam_trans']
ROOT_URLCONF = 'natcam_trans.urls'
LANGUAGES = (('pl', 'Polski'), ('en', 'English'), ('sk', 'Slovak'))
LANGUAGE_CODE = 'en'

