# -*- coding: utf-8 -*-
#
# by natcam.pl - based on http://code.google.com/p/django-transmeta/
#
import copy

from django.db import models
from django.utils.translation import string_concat
from django.utils import translation
from django.db.models.fields import NOT_PROVIDED
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.functional import Promise

from natcam_trans.utils import fallback_language, get_translated_name,\
                               get_all_translated_names

LANGUAGE_CODE = 0
LANGUAGE_NAME = 1

def default_value(field):
    '''
    When accessing to the name of the field itself, the value
    in the current language will be returned. For example:
      default language == 'sk'
      obj.name
         -> returns obj.name_sk

    If there is no value for field in default language then try to find first 
    language that accessed field has value for.
    '''

    def default_value_func(self):
        language = translation.get_language()
        nt_translate_fallback = getattr(self, 'nt_translate_fallback')

        tr_name = get_translated_name(field, language)
        ret = getattr(self, tr_name, None)

        if not ret and nt_translate_fallback:
            # there is no value in requested language try to find one in 
            # different language
            tr_name = get_translated_name(field, fallback_language())
            ret = getattr(self, tr_name)
            if not ret:
                for tr_name in get_all_translated_names(field):
                    ret = getattr(self, tr_name)
                    if ret:
                        return ret
        return ret
    return default_value_func

class NatcamTransMeta(models.base.ModelBase):

    def __new__(cls, name, bases, attrs):
        abstract_model_bases = []
        # get translable fields from Meta
        if 'Meta' in attrs and hasattr(attrs['Meta'], 'nt_translate'):
            nt_translate_fields = attrs['nt_translate'] = \
                                                      attrs['Meta'].nt_translate
            delattr(attrs['Meta'], 'nt_translate')

            nt_translate_fallback = True
            if hasattr(attrs['Meta'], 'nt_translate_fallback'):
                nt_translate_fallback = attrs['Meta'].nt_translate_fallback
                delattr(attrs['Meta'], 'nt_translate_fallback')
            attrs['nt_translate_fallback'] = nt_translate_fallback

            translate_verbose_names = True
            if hasattr(attrs['Meta'], 'translate_verbose_names'):
                translate_verbose_names = attrs['Meta'].translate_verbose_names
                delattr(attrs['Meta'], 'translate_verbose_names')
            
            default_language = fallback_language()

            # retrieve all fields from current class and its base classes
            all_fields = dict((attr_name, attr) 
                              for attr_name, attr \
                               in attrs.iteritems() \
                               if isinstance(attr, models.fields.Field))

            non_abstract_model_bases = []

            for base in bases:
                if hasattr(base, '_meta') and base._meta.abstract:
                    abstract_model_bases.append(base)
                else:
                    non_abstract_model_bases.append(base)
                
            for base in abstract_model_bases:
                all_fields.update(
                    dict((field.name, field) for field in base._meta.fields)
                )

            nt_translate_fields_2_clean = list(nt_translate_fields)

            # for each translable field add new one with suffix: 
            # _<LANGUAGE_CODE>, eg. title_pl
            for field in nt_translate_fields:
                if not field in all_fields:
                    raise ImproperlyConfigured(
                        "There is no field %(field)s in model %(name)s, "\
                        "as specified in Meta's translate attribute" % \
                        dict(field=field, name=name))
                original_attr = all_fields[field]
                
                # create translated field for every language specified in 
                # settings
                for lang in settings.LANGUAGES:
                    lang_code = lang[LANGUAGE_CODE]
                    lang_attr = copy.deepcopy(original_attr)
                    lang_attr.original_fieldname = field
                    lang_attr_name = get_translated_name(field, lang_code)
                    if lang_code != default_language:
                        # only will be required for default language
                        if not lang_attr.null \
                           and lang_attr.default is NOT_PROVIDED:
                            lang_attr.null = True
                        if not lang_attr.blank:
                            lang_attr.blank = True

                    # check if it's a Promise instance for lazy translations
                    # without it translations machinery would try to find translation by importing all models etc.
                    # which might cause circular import problem
                    if (isinstance(lang_attr.verbose_name, Promise) or lang_attr.verbose_name) and translate_verbose_names:
                        lang_attr.verbose_name = \
                                string_concat(lang_attr.verbose_name, 
                                              u' (%s)' % lang_code)
                    attrs[lang_attr_name] = lang_attr

                # if field that is to be translated is defined in current model,
                # then its definition is in attrs
                # we're removing it as it will clash with getter, 
                # eg. when field is not null and getter returns
                # value for different language this would cause error
                if field in attrs:
                    del attrs[field]
                    nt_translate_fields_2_clean.remove(field)
                    attrs[field] = property(default_value(field))

            # just as above, remove all translatable fields from base clases
            if nt_translate_fields_2_clean:
                for base in abstract_model_bases:
                    old_local_fields = base._meta.local_fields
                    new_local_fields = []
                    for locfield in base._meta.local_fields:
                        if locfield.name in nt_translate_fields_2_clean:
                            nt_translate_fields_2_clean.remove(locfield.name)
                            attrs[locfield.name] = \
                                          property(default_value(locfield.name))
                        else:
                            new_local_fields.append(locfield)
                    base._meta.local_fields = new_local_fields
                    setattr(base._meta, 'old_local_fields', old_local_fields)
                    if not nt_translate_fields_2_clean:
                        break

            bases = tuple(abstract_model_bases + non_abstract_model_bases + \
                          [NatcamDynamicTranslation])
        new_class = super(NatcamTransMeta, cls).__new__(cls, name, bases, attrs)

        for base in abstract_model_bases:
            old_local_fields = getattr(base._meta, 'old_local_fields', None)
            if hasattr(base._meta, '_field_cache'):
                del base._meta._field_name_cache
                del base._meta._field_cache
            if old_local_fields:
                base._meta.local_fields = old_local_fields
        return new_class


class NatcamDynamicTranslation(object):

    def order_fields(self, order_list):
        """ Useful for setting self.fields.keyOrder in form's __init__ 
            if you want to display all translated fields.
            For example if you have title_pl and title_en and price fields 
            you can use (in ModelForm __init__):

                class MyForm(ModelForm):
                    def __init__(self, *args, **kwargs):
                        self.fields.keyOrder =  \
                                self.instance.order_fields(['title', 'price'])

            and it will be automatically expanded to:
                ['title_pl', 'title_en', 'price']

            Order of fields is based on settings.LANGUAGES        
        """
        ord_list = copy.copy(order_list)
        for fieldname in self.nt_translate:
            try:
                idx = ord_list.index(fieldname)
                names = get_all_translated_names(fieldname)
                names.reverse()
                ord_list.remove(fieldname)
                [ord_list.insert(idx, trans_name) for trans_name in names]
            except ValueError:
                pass
        return ord_list
