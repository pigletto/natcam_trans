# -*- coding: utf-8 -*-

from django.template import Library
from django import template
from django.utils import translation
from django.utils.safestring import mark_safe

from natcam_trans.utils import get_all_translated_names, get_translated_name, get_languages_list

register = Library()


class NTFieldsNode(template.Node):
    def __init__(self, form, field_name, nodelist):
        self.form = template.Variable(form)
        self.field_name = template.Variable(field_name)
        self.request = template.Variable('request')
        self.nodelist = nodelist

    def render(self, context):
        """ prepare list of translation fields (Form) for specific field name
        """
        form = self.form.resolve(context)
        field_name = self.field_name.resolve(context)

        out = [form[tname] for tname in get_all_translated_names(field_name)]
        context['%s_translations' % field_name] = out

        #curr_lang = translation.get_language()

        output = []
        for lang in get_languages_list():
            tname = get_translated_name(field_name, lang)
            context['tfield'] = form[tname]
            #translation.activate(lang)
            output.append(self.nodelist.render(context))

        #translation.activate(curr_lang)
        return ''.join(output)


from django import template
@register.tag(name="translatedfields")
def do_set_translated_fields(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, form, field_name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    nodelist = parser.parse(('endtranslatedfields',))
    parser.delete_first_token()
    return NTFieldsNode(form, field_name, nodelist)


@register.simple_tag
def get_translation_languages_js():
    langlist = ','.join(["'%s'" % lang for lang in get_languages_list()])
    return mark_safe('var TRANSLATION_LANGUAGES = [%s];' % langlist)
