# -*- coding: utf-8 -*-
from django.utils import translation
from unittest import TestCase

from django.db import models
from natcam_trans.models import NatcamTransMeta

class TestClass1(models.Model):
    __metaclass__ = NatcamTransMeta
    title = models.CharField(u'Title', max_length=45)

    class Meta:
        nt_translate = ('title', )
        nt_translate_fallback = True
        translate_verbose_names = True

class TestNatcamTransMeta(TestCase):
    def setUp(self):
        self.obj = TestClass1()

    def test_attribute_cration_en(self):
        self.assertTrue(self.obj.title is None)

    def test_attribute_cration_pl(self):
        self.assertTrue(self.obj.title_pl is None)

    def test_attribute_cration_sk(self):
        self.assertTrue(self.obj.title_sk is None)

    def test_default_fallback(self):
        obj = TestClass1(title_en='english', title_pl='polish', title_sk='slovak')
        translation.activate('pl')
        self.assertEquals(obj.title, 'polish')

        translation.activate('sk')
        self.assertEquals(obj.title, 'slovak')

        translation.activate('en')
        self.assertEquals(obj.title, 'english')

    def test_default_fallback_2(self):
        """
            Check if empty or none field fallbacks properly to another language
        """
        obj = TestClass1(title_en='english', title_pl='polish', title_sk='')
        translation.activate('pl')
        self.assertEquals(obj.title, 'polish')

        # default fallback to default language
        translation.activate('sk')
        self.assertEquals(obj.title, 'english')

        translation.activate('en')
        self.assertEquals(obj.title, 'english')

        obj = TestClass1(title_en='', title_pl='polish', title_sk='')
        translation.activate('pl')
        self.assertEquals(obj.title, 'polish')

        translation.activate('sk')
        self.assertEquals(obj.title, 'polish')

