# -*- coding: utf-8 -*-
#
# by natcam.pl - based on http://code.google.com/p/django-transmeta/
#
from django.conf import settings
from django.utils import translation


def get_translated_name(name, language=None):
    """ Returns name with suffix for specific language
    """
    if not language:
        language = translation.get_language()
    return '%s_%s' % (name, language)

def get_untranslated_name(name, language=None):
    """ given name with language suffix eg. title_pl returns
        title
    """
    if not language:
        language = fallback_language()
    else:
        language = sanitize_language(language)
    lang_part = '_%s' % (language)
    idx = name.rfind(lang_part)

    if idx > 0:
        return name.replace(lang_part, '')

    return name

def get_all_translated_names(name):
    """ Returns all language versions of specific name
    """
    out = []
    for lang_code in get_languages_list():
        out.append(get_translated_name(name, language=lang_code))
    return out

def get_languages_list():
    """ Returns language codes as list
        Don't use cache here as it locks itself!
    """
    langs = getattr(settings, 'LANGUAGES_LIST', None)
    if not langs:
        langs = [language[0] for language in settings.LANGUAGES]
    return langs

def sanitize_language(language):
    """ changes language to two letter code and then to primary language 
        if lang is not supported
    """
    lang = language[:2]

    if lang not in get_languages_list():
        lang = fallback_language()
    return lang

def fallback_language():
    """ returns fallback language """
    return getattr(settings, 'TRANSMETA_DEFAULT_LANGUAGE', \
                   settings.LANGUAGE_CODE)
