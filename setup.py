from setuptools import setup, find_packages

setup(
    name = "natcam_trans",
    version = "1.0",
    url = 'http://github.com/pigletto/natcam_trans',
    license = 'MiT',
    description = "Content translations for Django - based on Django Transmeta",
    author = 'Natcam',
    packages = find_packages('src'),
    package_dir = {'': 'src'},
    install_requires = ['setuptools'],
)

